﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Web.Framework.Themes;

namespace Nop.Plugin.Payments.Redsys.ViewEngines
{
    public class CustomViewEngine : ThemeableRazorViewEngine
    {
        public CustomViewEngine()
        {
            PartialViewLocationFormats =
                new[]
                {
                    "~/Plugins/Payments.Redsys/Views/PaymentRedsys/{0}.cshtml"
                };
            ViewLocationFormats =
                new[]
                {
                    "~/Plugins/Payments.Redsys/Views/PaymentRedsys/{0}.cshtml"
                };
        }
    }
}
