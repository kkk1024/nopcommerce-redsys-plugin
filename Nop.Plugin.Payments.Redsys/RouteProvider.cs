﻿using System.Web.Mvc;
using System.Web.Routing;
using Nop.Web.Framework.Mvc.Routes;
using Nop.Plugin.Payments.Redsys.ViewEngines;

namespace Nop.Plugin.Payments.Redsys
{
    public class RouteProvider : IRouteProvider
    {
        public void RegisterRoutes(RouteCollection routes)
        {
            System.Web.Mvc.ViewEngines.Engines.Add(new CustomViewEngine());

            //Return
            routes.MapRoute("Plugin.Payments.Redsys.Return",
                 "Plugins/PaymentRedsys/Return",
                 new { controller = "PaymentRedsys", action = "Return" },
                 new[] { "Nop.Plugin.Payments.Redsys.Controllers" }
            );

            //Error
            routes.MapRoute("Plugin.Payments.Redsys.Error",
                 "Plugins/PaymentRedsys/RedsysError",
                 new { controller = "PaymentRedsys", action = "RedsysError" },
                 new[] { "Nop.Plugin.Payments.Redsys.Controllers" }
            );
        }

        public int Priority
        {
            get
            {
                return 0;
            }
        }
    }
}
