using Nop.Core.Configuration;

namespace Nop.Plugin.Payments.Redsys
{
    public class RedsysPaymentSettings : ISettings
    {

        internal const string DefaultTestUrl = "https://sis-t.redsys.es:25443/sis/realizarPago";
        internal const string DefaultTestEncryptionKey = "sq7HjrUOBfKmC576ILgskD5srU870gJ7";
        internal const string DefaultUrl = "https://sis.redsys.es/sis/realizarPago";

        public string Url { get; set; }
        public string TestUrl { get; set; }

        public string MerchantCode
        {
            get;
            set;
        }
        public string OrderPrefix
        {
            get;
            set;
        }
        public string Terminal { get; set; }
        public string MerchantName { get; set; }
        public string EncryptionKey { get; set; }
        public string TestEncryptionKey { get; set; }
        public bool IsTestMode { get; set; }
        public string PaymentReferenceText { get; set; }

        public decimal AdditionalFee { get; set; }
        public bool AdditionalFeePercentage
        {
            get; set;
        }

        public string SignatureVersion
        {
            get
            {
                return "HMAC_SHA256_V1";
            }
        }

        public string CurrentEncriptionKey
        {
            get
            {
                return IsTestMode ? TestEncryptionKey : EncryptionKey;
            }
        }

        public string CurrentUrl
        {
            get
            {
                return IsTestMode ? TestUrl : Url;
            }
        }        
    }
}
