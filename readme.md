#nopCommerce + Redsys payment plugin
This plugin allows nopCommerce to use the Redsys Payment Gateway
The plugin uses the SHA256 encryption for the new gateway version

## Installation

Add this project to your plugins solution and compile or copy the Payments.Redsys folder to your plugins folder

## Views Customization

This plugin adds a custom view engine that allows you to overwrite all the views used by the plugin in your custom theme.
Inside your custom theme folder, you need to put all your customized views at /Plugins/Payments.Redsys/Views/PaymentRedsys folder

Developed by [comodoo](https://www.comodoo.es)